package ro.zizicu.db;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import ro.zizicu.beans.PersonBean;
import ro.zizicu.workers.PersonsProcessor;

import java.sql.SQLException;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;


public class DBTests {

    @Test
    void testSelectPersons() {
        DatabaseUtils databaseUtils = new DatabaseUtils();
        List<PersonBean> personBeans =
                databaseUtils.executeSelect("select id, primary_name , primary_profession , known_for_titles from persons where id between ? and ?", new Integer[]{7829130, 7829150}, rs -> {
                    try {
                        return new PersonBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                });
        assertEquals(personBeans.size(), 21);
    }

    @Test
    void testSelectPersonIds() {
        DbOperations dbOperations = new DbOperations();
        List<Integer> personIds = dbOperations.readPersonIds();
        assertFalse(personIds.isEmpty());
    }

    @Test
    void testInsertProfession() {
        DbOperations dbOperations = new DbOperations();
        Integer id = dbOperations.insertProfession("test_profession");
        assertTrue(id > 0);
    }

    @Test
    void testPersonProcessor() {
        PersonsProcessor personsProcessor = new PersonsProcessor(7829130, 7829150);
        personsProcessor.process();
    }

    @AfterAll
    static void cleanUp() {
        DbOperations dbOperations = new DbOperations();
        dbOperations.cleanUp();
    }

}
