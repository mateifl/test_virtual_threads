package ro.zizicu.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

interface Mapper<T> {

    T map(ResultSet rs) throws SQLException;

}

interface UpdateMapper<T> {
    void map(PreparedStatement ps, T object) throws SQLException;
}

class ObjectArrayUpdateMapper implements UpdateMapper<Object[]> {
    @Override
    public void map(PreparedStatement ps, Object[] objectArray) throws SQLException {
        for (int i = 0; i < objectArray.length; i++) {
            switch (objectArray[i]) {
                case String s -> ps.setString(i + 1, s);
                case Integer ii -> ps.setInt(i + 1, ii);
                default -> throw new IllegalStateException("Unexpected type: " + objectArray[i].getClass().getName());
            }
        }
    }
}