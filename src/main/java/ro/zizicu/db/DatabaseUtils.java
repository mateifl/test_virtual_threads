package ro.zizicu.db;


import lombok.extern.slf4j.Slf4j;
import ro.zizicu.util.TestPropertiesReader;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Slf4j
public class DatabaseUtils {

    private Connection getConnection() {
        try {
            log.info("open connection");
            Connection connection = DriverManager.getConnection(
                    TestPropertiesReader.getInstance().getProperties().getProperty("db_url"),
                    TestPropertiesReader.getInstance().getProperties().getProperty("db_user"),
                    TestPropertiesReader.getInstance().getProperties().getProperty("db_password"));
            log.info("connection opened");
            return connection;
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <T> List<T> executeSelect(String sql, Object[] parameters, Mapper<T> mapper) {
        try {
            Connection connection = getConnection();
            log.debug("execute: {}", sql);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < parameters.length; i++) {
                switch(parameters[i]) {
                    case String str -> preparedStatement.setString(i + 1, str);
                    case Integer ii ->  preparedStatement.setInt(i + 1, ii);
                    default -> throw new IllegalStateException("Unexpected value: " + parameters[i]);
                }
            }
            ResultSet rs = preparedStatement.executeQuery();
            List<T> result = new ArrayList<>();
            while(rs.next()) {
                result.add(mapper.map(rs));
            }
            rs.close();
            preparedStatement.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> List<T> executeSelect(String sql, Mapper<T> mapper) {
        try {
            Connection connection = getConnection();
            log.debug("execute: {}", sql);
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            List<T> result = new ArrayList<>();
            while(rs.next()) {
                result.add(mapper.map(rs));
            }
            rs.close();
            statement.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> List<Integer> executeBatch(String sql, List<T> batchRecords, UpdateMapper<T> mapper) {
        log.info("execute batch {}", batchRecords.size());
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            for( T object : batchRecords ) {
                mapper.map(preparedStatement, object);
                preparedStatement.addBatch();
            }
            int[] results = preparedStatement.executeBatch();
            List<Integer> result = Arrays.stream(results).filter(i -> i != 1).boxed().toList();
            preparedStatement.close();
            log.debug("closing connection");
            connection.close();
            log.info("batch executed");
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void executeUpdate(String sql) {
        log.debug("execute update {}", sql);
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            statement.execute(sql);
            statement.close();
            log.debug("closing connection");
            connection.close();
            log.debug("update executed");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}