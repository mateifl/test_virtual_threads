package ro.zizicu.db;

import lombok.extern.slf4j.Slf4j;

import ro.zizicu.beans.PersonBean;
import ro.zizicu.beans.ProfessionBean;
import ro.zizicu.util.TestPropertiesReader;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class DbOperations {

    private final DatabaseUtils databaseUtils = new DatabaseUtils();

    public List<PersonBean> readPersons(String sql, Integer fromId, Integer toId) {
        log.debug("read persons records {}", fromId);
        return databaseUtils.executeSelect(sql, new Integer[]{fromId, toId}, rs -> {
            return new PersonBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
       });
    }


    public List<Integer> readPersonIds() {
        log.info("load persons ids");
        Mapper<Integer> mapper = rs -> rs.getInt(1);
        List<Integer> result = databaseUtils.executeSelect("select min(id) from persons", mapper);
        int minId = result.getFirst();
        int maxId = minId + Integer.parseInt(TestPropertiesReader.getInstance().getProperties().getProperty("total_records_loaded"));
        return databaseUtils.executeSelect("select id from persons where id between ? and ?", new Integer[]{minId, maxId}, mapper);
    }


    public Map<String, Integer> readProfessions() {
        log.info("load professions");
        List<ProfessionBean> professions = databaseUtils.executeSelect( "select id, profession from professions", rs ->
            new ProfessionBean(rs.getInt(1), rs.getString(2))
        );
        return professions.stream().collect(Collectors.toMap(ProfessionBean::profession, ProfessionBean::id));
    }

    public Integer insertProfession(String profession) {
        try {
            log.info("insert profession {}", profession);
            String sql = "insert into professions(profession) values('" + profession + "')";
            log.debug(sql);
            databaseUtils.executeBatch(sql, List.of(profession), (UpdateMapper<String>) (ps, object) -> ps.setString(1, profession));
            log.info("profession {} inserted", profession);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return databaseUtils.executeSelect( "select id from professions where profession = ?", new String[]{profession}, rs -> rs.getInt(1) ).getFirst();

    }

    public List<Integer> insertPersonProfessionRecords(List<Object[]> records) {
        log.debug("insert records in person profession link");
        List<Integer> result = databaseUtils.executeBatch("insert into persons_professions(profession_id, person_id) values(?, ?)",
                records,
                new ObjectArrayUpdateMapper());
        return result;
    }

    public void cleanUp() {
        databaseUtils.executeUpdate("delete from persons_professions");
        databaseUtils.executeUpdate("delete from professions");
    }

}
