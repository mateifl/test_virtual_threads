package ro.zizicu;

import lombok.extern.slf4j.Slf4j;
import ro.zizicu.db.DbOperations;
import ro.zizicu.runners.PoolVirtualThreadsRunner;
import ro.zizicu.runners.ThreadsRunner;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Slf4j
public class MainVirtual {

    public static void main(String[] args) {
        DbOperations operations = new DbOperations();
        List<Integer> personsIds = operations.readPersonIds();
        Instant start = Instant.now();
        ThreadsRunner threadsRunner = new PoolVirtualThreadsRunner(personsIds);
        threadsRunner.runThreads();
        Instant end = Instant.now();
        log.info("time: {}", Duration.between(start, end).toMillis());
    }
}




