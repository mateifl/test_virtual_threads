package ro.zizicu;

import lombok.extern.slf4j.Slf4j;
import ro.zizicu.db.DbOperations;
import ro.zizicu.runners.SemaphoredVirtualThreadsRunner;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Slf4j
public class MainVirtualSemaphored {

    public static void main(String[] args) {
        log.info("run virtual threads");
        DbOperations operations = new DbOperations();
        List<Integer> personsIds = operations.readPersonIds();
        Instant now = Instant.now();
        SemaphoredVirtualThreadsRunner semaphoredVirtualThreadsRunner = new SemaphoredVirtualThreadsRunner(personsIds);
        semaphoredVirtualThreadsRunner.runThreads();
        Instant end = Instant.now();
        log.info("time: {}", Duration.between(now, end).getSeconds());
    }
}
