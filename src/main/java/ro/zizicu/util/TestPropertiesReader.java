package ro.zizicu.util;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Getter
@Slf4j
public class TestPropertiesReader {

    private static TestPropertiesReader instance;
    Properties properties;
    private TestPropertiesReader() {
        properties = new Properties();
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("test.properties");
            properties.load(is);
            log.info("properties loaded {}", properties.getProperty("db_url"));
        }
        catch(IOException e) {
            log.error("", e);
        }
    }

    public static TestPropertiesReader getInstance() {
        synchronized (TestPropertiesReader.class) {
            if (instance == null)
                instance = new TestPropertiesReader();
        }
        return instance;
    }

}
