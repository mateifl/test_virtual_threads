package ro.zizicu;

import lombok.extern.slf4j.Slf4j;
import ro.zizicu.db.DbOperations;
import ro.zizicu.runners.NativeThreadsRunner;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Slf4j
public class Main {

    public static void main(String[] args) {
        log.info("run native threads");
        DbOperations operations = new DbOperations();
        List<Integer> personsIds = operations.readPersonIds();
        Instant now = Instant.now();
        NativeThreadsRunner threadsRunner = new NativeThreadsRunner(personsIds);
        threadsRunner.runThreads();
        Instant end = Instant.now();
        log.info("time: {}", Duration.between(now, end).getSeconds());
    }
}