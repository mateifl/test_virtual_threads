package ro.zizicu.runners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.zizicu.util.TestPropertiesReader;
import ro.zizicu.workers.ProcessorTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


@Slf4j
public class SemaphoredVirtualThreadsRunner extends AbstractThreadsRunner {

    public SemaphoredVirtualThreadsRunner(List<Integer> personIdList) {
        super(personIdList);
    }


    @Override
    protected ExecutorService createExecutor() {
        return Executors.newVirtualThreadPerTaskExecutor();
    }

    @Override
    protected Semaphore createSemaphore() {
        return new Semaphore(numberOfThreads);
    }
}
