package ro.zizicu.runners;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.zizicu.util.TestPropertiesReader;
import ro.zizicu.workers.ProcessorTask;

import java.util.List;
import java.util.concurrent.*;

@Slf4j
public class NativeThreadsRunner extends AbstractThreadsRunner {

    public NativeThreadsRunner(List<Integer> personIdList) {
        super(personIdList);
    }

    @Override
    protected ExecutorService createExecutor() {
        return Executors.newFixedThreadPool(numberOfThreads);
    }

    @Override
    protected Semaphore createSemaphore() {
        return null;
    }
}
