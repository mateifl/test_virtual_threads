package ro.zizicu.runners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.zizicu.util.TestPropertiesReader;
import ro.zizicu.workers.ProcessorTask;

import java.util.List;
import java.util.concurrent.*;

@Slf4j
@RequiredArgsConstructor
public class PoolVirtualThreadsRunner implements ThreadsRunner {
    private final List<Integer> personIdList;

    @Override
    public void runThreads() {
        try {
            ThreadFactory factory = Thread.ofVirtual().name("Virtual-thread", 1).factory();
            int numberOfThreads = Integer.parseInt(TestPropertiesReader.getInstance().getProperties().getProperty("threads"));
            log.info("threads: {}", numberOfThreads);
            ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads, factory);
            int totalRecords = personIdList.size();
            int partitions = totalRecords / 1000 - 5;
            log.info("partitions: {}", partitions);
            for (int i = 0; i < partitions; i++) {

                int startIndex = 1000 * i;
                int endIndex = 1000 * (i + 1) - 1;
                log.debug("{} {}", personIdList.get(startIndex), personIdList.get(endIndex));
                ProcessorTask task = new ProcessorTask(personIdList.get(startIndex), personIdList.get(endIndex), null);
                executorService.submit(task);
            }
            boolean r = executorService.awaitTermination(2000, TimeUnit.SECONDS);
            log.info("finished: {}", r);
            executorService.shutdown();
        }
        catch(InterruptedException e) {
            log.error("", e);
        }
    }
}
