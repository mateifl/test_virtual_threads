package ro.zizicu.runners;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.zizicu.util.TestPropertiesReader;
import ro.zizicu.workers.ProcessorTask;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Slf4j
public abstract class AbstractThreadsRunner implements ThreadsRunner {

    private final List<Integer> personsIds;

    private Integer partitions;
    private Integer batchSize;
    protected Integer numberOfThreads;

    protected void init() {
        batchSize = Integer.parseInt(TestPropertiesReader.getInstance().getProperties().getProperty("batch_size"));
        int totalRecords = personsIds.size();
        partitions = totalRecords/batchSize - 1;
        log.info("partitions: {}", partitions);
        numberOfThreads = Integer.parseInt(TestPropertiesReader.getInstance().getProperties().getProperty("threads"));
        log.info("threads: {}", numberOfThreads);
    }

    protected abstract ExecutorService createExecutor();
    protected abstract Semaphore createSemaphore();

    public void runThreads() {
        try {
            log.info("number of records to process: {}", personsIds.size());
            init();
            ExecutorService executorService = createExecutor();
            Semaphore semaphore = createSemaphore();
            for (int i = 0; i < partitions; i++) {
                int startIndex = batchSize * i;
                int endIndex = batchSize * (i + 1) - 1;
                log.debug("{} {}", personsIds.get(startIndex), personsIds.get(endIndex));
                ProcessorTask task = new ProcessorTask(personsIds.get(startIndex), personsIds.get(endIndex), semaphore);
                executorService.submit(task);
            }
            executorService.shutdown();
            boolean r = executorService.awaitTermination(600, TimeUnit.SECONDS);
            log.info("finished: {}", r);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
    }
}
