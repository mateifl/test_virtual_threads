package ro.zizicu.beans;

public record PersonProfessionBean(Integer id, Integer professionId, Integer personId) {
}
