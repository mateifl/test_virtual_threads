package ro.zizicu.beans;

public record ProfessionBean(int id, String profession)  {
}
