package ro.zizicu.beans;

public record PersonBean(Integer id, String name, String profession, String knownForTitles)  {

}
