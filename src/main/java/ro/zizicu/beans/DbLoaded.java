package ro.zizicu.beans;

import java.sql.ResultSet;

public interface DbLoaded {

    void fromResultSet(ResultSet rs);

}
