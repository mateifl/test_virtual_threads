package ro.zizicu.beans;

public interface EntityWithId {
    Integer getId();
    void setId(Integer id);
}
