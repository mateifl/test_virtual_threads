package ro.zizicu.workers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.zizicu.beans.PersonBean;
import ro.zizicu.db.DbOperations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Slf4j
public class PersonsProcessor {

    private final DbOperations dbOperations = new DbOperations();
    private final Integer startId;
    private final Integer endId;

    public List<Integer> process() {
        try {
            log.info("start database processing {} {}", startId, endId);
            Map<String, Integer> professions = dbOperations.readProfessions();
            log.debug("loaded processions {}", professions.size());
            List<PersonBean> persons = dbOperations.readPersons("select id, primary_name , primary_profession , known_for_titles  from persons where id between ? and ?", startId, endId);
            List<Object[]> personProfessionLink = new ArrayList<>();
            for(PersonBean personBean : persons) {
                String professionsString = personBean.profession();
                String[] professionArray = professionsString.split(",");
                for(String profession : professionArray) {
                    if(professions.containsKey(profession)) {
                        personProfessionLink.add(new Integer[] {professions.get(profession) , personBean.id()});
                    }
                    else{
                        Integer professionId = dbOperations.insertProfession(profession);
                        if(professionId == -1)
                            professionId = professions.get(profession);
                        personProfessionLink.add(new Integer[] {professionId , personBean.id()});
                        professions.put(profession, professionId);
                    }
                }
            }
            List<Integer> result = dbOperations.insertPersonProfessionRecords(personProfessionLink);
            log.info("end database processing {} {}", startId, endId);
            return result;
        } catch (Exception e) {
            log.error("", e);
            return null;
        }
    }
}
