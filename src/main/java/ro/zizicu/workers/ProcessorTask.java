package ro.zizicu.workers;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;

@Slf4j
@RequiredArgsConstructor
@Getter
public class ProcessorTask implements Callable<List<Integer>> {

    private final Integer startId;
    private final Integer endId;
    private final Semaphore semaphore;

    @Override
    public List<Integer> call() {
        Thread.currentThread().setName("" + startId);
        try {
            if(semaphore != null)
                semaphore.acquire();
            PersonsProcessor personsProcessor = new PersonsProcessor(startId, endId);
            return personsProcessor.process();
        } catch (InterruptedException e) {
            log.error("", e);
            return null;
        } finally {
            if(semaphore != null)
                semaphore.release();
        }
    }
}
